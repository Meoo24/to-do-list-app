<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Task;
use Response;

class TaskController extends Controller
{
    public function index() {

        // return Task::orderBy('id', 'DESC')->get();
        return Task::all();
    }

    public function store(Request $request) {
        
        $task = new Task([
            'title' => $request->title,
            'item' => $request->item,
        ]);

        if($request->hasfile('image_url')) {
            $file = $request->file('image_url');
            $ext = $file->getClientOriginalExtension();
            $filename = time().'.'.$ext;
            $file->move('uploads/tasks/', $filename);
            $task->image_url = $filename;
        }
        $task->save();
        return ($task);
    }

    public function upload(Request $request, $id) {

        $task = Task::find($id);
        // if($request->hasfile('image_url')) {
        //     $file = $request->file('image_url');
        //     $ext = $file->getClientOriginalExtension();
        //     $filename = time().'.'.$ext;
        //     $file->move('uploads/tasks/', $filename);
        //     $task->image_url = $filename;
        // }
        $file = $request->file('image_url');
        $ext = $file->getClientOriginalExtension();
        $filename = time().'.'.$ext;
        $file->move('uploads/tasks/', $filename);
        $task->image_url = $filename;
        $task->update();
        return ($task);

    }

    public function edit(Request $request, $id) {
        $task = Task::find($id);
        return ($task);
    }   

    public function update(Request $request, $id) {

        $task = Task::find($id);
        $task->title = $request->title;
        $task->item = $request->item;
        $task->update();
        return($task);
    }

    public function delete($id) {

        $task = Task::find($id);
        // error_log($task);
        $task->delete();
        return ('Task has been deleted');
    }
}
