<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';

    public $primarkey = 'id';

    protected $fillable = [
        'title', 
        'item', 
        'image_url',
    ];

    public $timestamps = false;

    // public function getNameAttribute() {
    //     return $this->title;
    // }
}
