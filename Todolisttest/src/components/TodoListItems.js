import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const TodoListItems = (props) => {
  return (
    <View style={styles.item}>
      <View style={styles.itemLeft}>
        <Text>{props.text}</Text>
      </View>
    </View>
  )
}

export default TodoListItems

const styles = StyleSheet.create({})