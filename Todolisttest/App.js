/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
  Alert,
  ImageBackground,
  Modal,
  Pressable,
  TextInput,
  Image,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import TodoListItems from './src/components/TodoListItems'

import axios from 'axios'
import { Formik } from 'formik';

import { launchImageLibrary } from 'react-native-image-picker';

const Section = ({children, title}): Node => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};


const App: () => Node = (props) => {

  const [taskItems, setTaskItems] = useState([])
  const [modalSave, setModalSave] = useState(false)

  const [titleInput, setTitleInput] = useState('')
  const [itemInput, setItemInput] = useState('')
  const [imageUpload, setImageUpload] = useState(null)
  const [inputData, setInputData] = useState()

  const [editData, setEditData] = useState()
  const [isEdit, setIsEdit] = useState(false)
  const [imageGallery, setImageGallery] = useState(null)

  const [imgPreview, setImgPreview] = useState(null)

  // render list or tasks
  async function getTaskList() {
    try {
      const tasks = await axios.get('http://10.0.2.2:8000/api/tasks/')
      setTaskItems(tasks.data)
      console.log(tasks.data)
    }catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getTaskList()
  },[])

  // SAVE HANDLER
  const saveData = async(values) => {

    if (isEdit) {
      const editTaskData = {
        id: editData.id,
        title: values.title,
        item: values.item,
        image_url: values.image_url,
      }
      try {
        await axios.put(`http://10.0.2.2:8000/api/update-task/${editData.id}/`, editTaskData).then(res => {
          // console.log(res.status)
          if(res.status === 200) {
            uploadHandler(editData.id)
          }
        })
      } catch(error) {
        console.log(error)
      }
      
    } else {

      try {
        await axios.post('http://10.0.2.2:8000/api/save/', values).then(res => {
          // console.log(res.data)
          uploadHandler(res.data.id)
        })
      }catch (error) {
        console.log(error)
      }

    }
  }

  const uploadHandler = async(id) => {
    // console.log(id)
    if(imageUpload !== null) {

      console.log('img upload function')

      const formData = new FormData()

      formData.append('image_url', {
        name: imageUpload.fileName,
        type: imageUpload.type,
        uri: Platform.OS === 'ios' ? imageUpload.uri.replace('file://', '') : imageUpload.uri,
      });

      let res = await fetch(
        `http://10.0.2.2:8000/api/upload/${id}`,
        {
          method: 'post',
          body: formData,
          headers: {
            'Content-Type': 'multipart/form-data; ',
          },
        }
      );
      let response = await res.status;
      if(response === 200) {
        setImageUpload(null)
        
      }
    }
    setModalSave(!modalSave)
    setImageGallery(null)
    setImgPreview(null)
    setEditData()
    getTaskList()
  }

  // EDIT handler
  const editTask = async(id) => {
    // console.log(id)
    try{
      await axios.get(`http://10.0.2.2:8000/api/edit-task/${id}/`).then(res => {
        const data = res.data
        // console.log(res.data)
        setEditData({
          id: data.id,
          title: data.title,
          item: data.item,
          image_url: data.image_url,
        })
        setIsEdit(true)
        setModalSave(true)
        setImgPreview({uri:'http://10.0.2.2:8000/uploads/tasks/' + data.image_url})
      })
    }catch(error) {
      console.log(error)
    }
  }
  

  // DELETE handler
  const deleteTask = async(id) => {
    try{
      await axios.delete(`http://10.0.2.2:8000/api/delete/${id}`).then(res => {
        getTaskList()
        setIsEdit(false)
      })
    }catch(error) {
      console.log(error)
    }
  }

  // toggle modal
  const closeModal = () => {
    setModalSave(!modalSave)
    setIsEdit(false)
    setEditData()
    setImageGallery(null)
    setImgPreview(null)
    setImageUpload(null)
  }

  const openGallery = () => {
    const options = {
      storageOptions: {
        path: 'images',
        mediaType: 'photo',
      },
      includeBase64: true,
    };
    // console.log(options)
    launchImageLibrary(options, response => {
      // console.log(response);
      if (response.didCancel) {
      console.log('User cancelled image picker');
      } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
      } else {
        // You can also display the image using data:
        const source = {uri: 'data:image/jpeg;base64,' + response.assets[0].base64};
        setImageGallery(source)
        setImgPreview(source)
        setImageUpload(response.assets[0])
        }
      });
  };

  const imageRender = (e) => {
    console.log(e)
  }

  return (
    <View style={styles.container}>

    <Modal
        animationType="slide"
        transparent={true}
        visible={modalSave}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalSave(!modalSave);
          setIsEdit(!isEdit);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>

            <Formik
              initialValues={{ 
                title: (editData && editData.title) || '',
                item: (editData && editData.item) || '',
              }}
              onSubmit={values => saveData(values)}
              onReset
            >
              {({ handleChange, handleBlur, handleSubmit, values }) => (
                <View>
                  <View style={styles.imgContainer}>
                    {
                      !isEdit ?
                      <Image
                        style={styles.tinyLogo}
                        source={imageGallery}
                      />
                      :
                      <Image
                        style={styles.tinyLogo}
                        source={imgPreview}
                      />
                    }
                  </View>
                  <View>
                    <Text>Title</Text>
                    <TextInput
                      style={styles.input}
                      onChangeText={handleChange('title')}
                      onBlur={handleBlur('title')}
                      value={values.title}
                    />
                  </View>
                  <Text>Description</Text>
                  <View>
                    <TextInput
                      style={styles.input}
                      onChangeText={handleChange('item')}
                      onBlur={handleBlur('item')}
                      value={values.item}
                    />
                  </View>

                  <View>
                    <Button
                      style={styles.btnUpload}
                      title={'Select Image'}
                      onPress={() => {
                      openGallery();
                      }}
                    />
                  </View>

                  <View style={styles.modalButtonStyle}>
                    <Pressable
                      style={[styles.button, styles.buttonAdd]}
                      onPress={handleSubmit}
                    >
                      <Text style={styles.textStyle}>
                        { !isEdit ? 'Add' : 'Edit'}
                      </Text>
                    </Pressable>
                  </View>
                
                  <View>
                    <Pressable
                      style={[styles.button, styles.buttonClose]}
                      onPress={() => closeModal()}
                    >
                      <Text style={styles.textStyle}>Close</Text>
                    </Pressable>
                  </View>

                </View>
              )}
            </Formik>

          </View>
        </View>
      </Modal>

      <View style={styles.colWrapper}>
        <Text style={styles.sectionTitle}>To do list</Text>
        <Button title='Add Task' onPress={() => {
          setModalSave(true)
          setIsEdit(false)
        }} />
          <ScrollView>

            <View style={styles.bodyStyle}>
              { taskItems?.map(i => {
                return (
                  <View key={i.id} style={styles.item}> 
                    <View>
                      <Text style={styles.itemTitle}>{i.title}</Text>
                      <Text>{i.item}</Text>
                      {/* <Text>uri: http://10.0.2.2:8000/uploads/tasks/{i.image_url}</Text> */}
                    </View>
                    <View>
                      <Image 
                        style={styles.viewImg}
                        source={{uri:'http://10.0.2.2:8000/uploads/tasks/' + i.image_url}}
                      />
                    </View>
                    <View >
                      <Pressable
                        style={[styles.button, styles.btnEdit]}
                        onPress={() => editTask(i.id)}
                      >
                        <Text>Edit</Text>
                      </Pressable>
                    {/* </View>
                    <View> */}
                      <Pressable
                        style={[styles.button, styles.btnRemove]}
                        onPress={() => deleteTask(i.id)}
                      >
                        <Text>Del</Text>
                      </Pressable>
                    </View>
                  </View>
                )
              })}
            </View>

          </ScrollView>
      </View>
      
    </View>

    // <SafeAreaView style={backgroundStyle}>
    //   <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
    //   <ScrollView
    //     contentInsetAdjustmentBehavior="automatic"
    //     style={backgroundStyle}>
    //     <Header />
    //     <View
    //       style={{
    //         backgroundColor: isDarkMode ? Colors.black : Colors.white,
    //       }}>
    //       <Section title="Step One">
    //         Edit <Text style={styles.highlight}>App.js</Text> to change this
    //         screen and then come back to see your edits.
    //       </Section>
    //     </View>
    //   </ScrollView>
    // </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E8EAED',
  },
  colWrapper: {
    paddingTop: 80,
    paddingHorizontal: 20,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  bodyStyle: {
    marginTop: 30,
  },
  item: {
    backgroundColor: '#FFF',
    padding: 15,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#eb5e34",
  },
  buttonAdd: {
    backgroundColor: '#10BBF1',
  },
  btnRemove: {
    backgroundColor: '#eb4034',
  },
  btnEdit:{
    backgroundColor: '#10BBF1',
    marginBottom: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalButtonStyle: {
    marginTop: 20,
    marginBottom: 20,
  },
  input: {
    height: 40,
    width: 200,
    margin: 8,
    borderWidth: 1,
    padding: 5,
  },
  titleText: {
    fontSize: 20,
    fontWeight: "bold"
  },
  btnUpload: {
    margin: 8,
    padding: 5,
  },
  imgContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  viewImg: {
    width: 50,
    height: 50,
  },
  itemTitle: {
    fontSize: 17,
    fontWeight: 'bold',
  }
  // sectionContainer: {
  //   marginTop: 32,
  //   paddingHorizontal: 24,
  // },
  
  // sectionDescription: {
  //   marginTop: 8,
  //   fontSize: 18,
  //   fontWeight: '400',
  // },
  // highlight: {
  //   fontWeight: '700',
  // },
});

export default App;
